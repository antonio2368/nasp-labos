public class NodeOperations {
    public static <T extends Comparable> Node<T> parent(Node<T> n) {
        return n.getParent();
    }

    public static <T extends Comparable> Node<T> grandParent(Node<T> n) {
        Node<T> p = n.getParent();

        if (p == null) {
            return null;
        }

        return p.getParent();
    }

    public static <T extends Comparable> Node<T> sibling(Node<T> n) {
        Node<T> p = n.getParent();
        if (p == null) {
            return Node.Leaf();
        }

        if (n == p.getLeft()) {
            return p.getRight();
        } else {
            return p.getLeft();
        }
    }

    public static <T extends Comparable> Node<T> uncle(Node<T> n) {
        Node<T> p = n.getParent();
        Node<T> g = grandParent(n);
        if (g == null) {
            return null;
        }

        return sibling(p);
    }

    public static <T extends Comparable> void rotateLeft(Node<T> n) {
        Node<T> nnew = n.getRight();
        Node<T> p = n.getParent();
        assert(!nnew.isLeaf());
        n.setRight(nnew.getLeft());
        nnew.setLeft(n);
        n.setParent(nnew);

        if (n.getRight() != null) {
            n.getRight().setParent(n);
        }

        if (p != null) {
            if (n == p.getLeft()) {
                p.setLeft(nnew);
            } else if (n == p.getRight()) {
                p.setRight(nnew);
            }
        }
        nnew.setParent(p);
    }

    public static <T extends Comparable> void rotateRight(Node<T> n) {
        Node<T> nnew = n.getLeft();
        Node<T> p = n.getParent();
        assert(!nnew.isLeaf());
        n.setLeft(nnew.getRight());
        nnew.setRight(n);
        n.setParent(nnew);

        if (n.getLeft() != null) {
            n.getLeft().setParent(n);
        }

        if (p != null) {
            if (n == p.getLeft()) {
                p.setLeft(nnew);
            } else if (n == p.getRight()) {
                p.setRight(nnew);
            }
        }

        nnew.setParent(p);
    }

    public static <T extends Comparable> Node<T> insert(Node<T> root, Node<T> n) {
        insertRecurse(root, n);

        insertRepairTree(n);

        root = n;

        while (parent(root) != null) {
            root = parent(root);
        }

        return root;
    }

    public static <T extends Comparable> void insertRecurse(Node<T> root, Node<T> n) {
        if (root != null && n.getValue().compareTo(root.getValue()) < 0) {
            if (!root.getLeft().isLeaf()) {
                insertRecurse(root.getLeft(), n);
                return;
            } else {
                root.setLeft(n);
            }
        } else if (root != null){
            if (!root.getRight().isLeaf()) {
                insertRecurse(root.getRight(), n);
                return;
            } else {
                root.setRight(n);
            }
        }

        n.setParent(root);
        n.setLeft(Node.Leaf());
        n.setRight(Node.Leaf());
        n.setRed(true);
    }

    public static <T extends Comparable> void insertRepairTree(Node<T> n) {
        if (n.getParent() == null) {
            n.setRed(false);
        } else if (!n.getParent().isRed()) {
            return;
        } else if (uncle(n) != null && uncle(n).isRed()) {
            n.getParent().setRed(false);
            uncle(n).setRed(false);
            grandParent(n).setRed(true);
            insertRepairTree(grandParent(n));
        } else  {
            Node<T> p = n.getParent();
            Node<T> g = grandParent(n);
            if (g.getLeft() != null && n == g.getLeft().getRight()) {
                rotateLeft(p);
                n = n.getLeft();
            } else if (g.getRight() != null && n == g.getRight().getLeft()) {
                rotateRight(p);
                n = n.getRight();
            }

            p = n.getParent();
            g = grandParent(n);

            if (n == p.getLeft()) {
                rotateRight(g);
            } else {
                rotateLeft(g);
            }

            p.setRed(false);
            g.setRed(true);
        }
    }

    public static <T extends Comparable> void replaceNode(Node<T> n, Node<T> child) {
        child.setParent(n.getParent());
        if (n.getParent() != null) {
            if (n == n.getParent().getLeft()) {
                n.getParent().setLeft(child);
            } else {
                n.getParent().setRight(child);
            }
        }
    }
    
    public static <T extends Comparable> Node<T> findNode(Node<T> root, Node<T> n) {
        if (root.isLeaf()) {
            return null;
        }

        if (root.getValue().compareTo(n.getValue()) == 0) {
            return root;
        }

        if (n.getValue().compareTo(root.getValue()) < 0) {
            return findNode(root.getLeft(), n);
        } else {
            return findNode(root.getRight(), n);
        }
    }

    public static <T extends Comparable> Node<T> findSmallestNode(Node<T> root) {
        if (root.isLeaf()) {
            return root;
        }

        if (root.getLeft().isLeaf()) {
            return root;
        }

        return findSmallestNode(root.getLeft());
    }

    public static <T extends Comparable> Node<T> delete(Node<T> root, Node<T> n) {
        if (root == null) {
            throw new IllegalArgumentException("Tree is empty");
        }

        Node<T> deleteNode = findNode(root, n);
        if (deleteNode == null) {
            throw new IllegalArgumentException("Node not found");
        }

        Node<T> smallest = findSmallestNode(deleteNode.getRight());
        if (!smallest.isLeaf()) {
            deleteNode.setValue(smallest.getValue());
            deleteNode = smallest;
        }

        deleteNode = deleteOneChild(deleteNode);

        while (deleteNode.getParent() != null) {
            deleteNode = deleteNode.getParent();
        }

        return deleteNode.isLeaf() ? null : deleteNode;
    }

    public static <T extends Comparable> Node<T> deleteOneChild(Node<T> n) {
        Node<T> child = n.getRight().isLeaf() ? n.getLeft() : n.getRight();

        replaceNode(n, child);
        if (!n.isRed()) {
            if (child.isRed()) {
                child.setRed(false);
            } else {
                deleteCase1(child);
            }
        }

        return child;
    }

    private static <T extends Comparable> void deleteCase1(Node<T> n) {
        if (n.getParent() != null) {
            deleteCase2(n);
        }
    }

    private static <T extends Comparable> void deleteCase2(Node<T> n) {
        Node<T> s = sibling(n);

        if (s.isRed()) {
            n.getParent().setRed(true);
            s.setRed(false);

            if (n == n.getParent().getLeft()) {
                rotateLeft(n.getParent());
            } else {
                rotateRight(n.getParent());
            }
        }
        deleteCase3(n);
    }

    private static <T extends Comparable> void deleteCase3(Node<T> n) {
        Node<T> s  = sibling(n);

        if (!n.getParent().isRed() &&
            !s.isRed() &&
            !s.getLeft().isRed() &&
            !s.getRight().isRed()) {
            s.setRed(true);
            deleteCase1(n.getParent());
        } else {
            deleteCase4(n);
        }
    }

    private static <T extends Comparable> void deleteCase4(Node<T> n) {
        Node<T> s = sibling(n);

        if (n.getParent().isRed() &&
            !s.isRed() &&
            !s.getLeft().isRed() &&
            !s.getRight().isRed()) {
            s.setRed(true);
            n.getParent().setRed(false);
        } else {
            deleteCase5(n);
        }
    }

    private static <T extends Comparable> void deleteCase5(Node<T> n) {
        Node<T> s = sibling(n);

        if (!s.isRed()) {
            if (n == n.getParent().getLeft() &&
                !s.getRight().isRed() &&
                s.getLeft().isRed()) {
                s.setRed(true);
                s.getLeft().setRed(false);
                rotateRight(s);
            } else if (n == n.getParent().getRight() &&
                        !s.getLeft().isRed() &&
                        s.getRight().isRed()) {
                s.setRed(true);
                s.getRight().setRed(false);
                rotateLeft(s);
            }
        }

        deleteCase6(n);
    }

    private static <T extends Comparable> void deleteCase6(Node<T> n) {
        Node<T> s = sibling(n);

        s.setRed(n.getParent().isRed());
        n.getParent().setRed(false);

        if (n == n.getParent().getLeft()) {
            s.getRight().setRed(false);
            rotateLeft(n.getParent());
        } else {
            s.getLeft().setRed(false);
            rotateRight(n.getParent());
        }
    }

}
