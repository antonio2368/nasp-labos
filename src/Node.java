public class Node<T extends Comparable> {
    private Node parent;
    private Node left;
    private Node right;
    private boolean isRed;
    private T value;

    public Node(T value) {
        this.value = value;
        if (value != null) {
            left = Leaf();
            right = Leaf();
        } else {
            isRed = false;
        }
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public Node<T> getParent() {
        return parent;
    }

    public void setParent(Node parent) {
        this.parent = parent;
    }

    public Node<T> getLeft() {
        return left;
    }

    public void setLeft(Node left) {
        this.left = left;
    }

    public Node<T> getRight() {
        return right;
    }

    public void setRight(Node right) {
        this.right = right;
    }

    public boolean isRed() {
        return isRed;
    }

    public void setRed(boolean red) {
        isRed = red;
    }

    public boolean isLeaf() {
        return value == null;
    }

    @Override
    public String toString() {
        return String.format("%s(%s)", value == null ? "LEAF" : value.toString(), isRed ? "R" : "B");
    }

    public static Node Leaf() {
        return new Node(null);
    }
}
