import java.io.*;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static boolean PRINT_LEAVES  = false;

    public static void main(String[] args) {
        Node<Integer> root = null;

        if (args.length != 0) {
            try (BufferedReader fileReader = new BufferedReader(new FileReader(args[0]));
                 Scanner in = new Scanner(fileReader)) {

                while (in.hasNextInt()) {
                    int i = in.nextInt();
                    Node<Integer> node = new Node(i);

                    root = NodeOperations.insert(root, node);

                    printTree(root, 0, PRINT_LEAVES);
                    System.out.println("------------------------");
                }
            } catch (FileNotFoundException e) {
                System.err.println("File '" + args[0] + "' was not found.");
                System.exit(0);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("Initial tree is empty");
        }

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
             Scanner in = new Scanner(reader)) {
            while (true) {
                System.out.println("Pick an option:");
                System.out.println("i - Insert new node");
                if (root != null) {
                    System.out.println("d - Delete a node");
                }
                System.out.println("e - exit");

                String choice = in.next();

                boolean exit = false;
                switch (choice.toLowerCase().trim()) {
                    case "i": {
                        root = insertNode(root, in);
                        break;
                    }
                    case "d": {
                        if (root == null) {
                            System.out.println("Invalid choice");
                            continue;
                        }
                        root = deleteNode(root, in);
                        break;
                    }
                    case "e": {
                        exit = true;
                        break;
                    }
                    default: {
                        System.out.println("Invalid choice");
                        continue;
                    }
                }

                if (exit) {
                    break;
                }

                printTree(root, 0, PRINT_LEAVES);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static <T extends Comparable> Node<T> insertNode(Node<T> root, Scanner in) {
        System.out.print("Node to insert: ");
        int value = in.nextInt();
        root = NodeOperations.insert(root, new Node(value));
        return root;
    }

    private static <T extends Comparable> Node<T> deleteNode(Node<T> root, Scanner in) {
        System.out.print("Node to delete: ");
        int value = in.nextInt();
        try {
            root = NodeOperations.delete(root, new Node(value));
        }
        catch (IllegalArgumentException e) {
            System.err.println(e.getMessage());
        }

        return root;

    }

    public static void printTree(Node<Integer> node, int level, boolean printLeaves) {
        if (node == null) {
            return;
        }

        if (printLeaves || !node.isLeaf()) {
            System.out.printf("%s%s%n", "  ".repeat(level), node);
        }

        printTree(node.getLeft(), level + 1, printLeaves);
        printTree(node.getRight(), level + 1, printLeaves);
    }
}
